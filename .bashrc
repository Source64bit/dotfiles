#
# .bashrc
#

# default configuration locate on /etc/skel/

# Modication for PS1 
PS1='\[\033[1;31m\][\[\033[1;32m\]\u\[\033[1;36m\]@\h\[\033[1;35m\] \w\[\033[1;31m\]]\[\033[1;33m\]$\[\033[0m\] '
export PS1;

# Source the alias on aliasrc
[ -f ~/.config/aliasrc ]
        source ~/.config/aliasrc

# change key input speed
xset r rate 300 50

# PATH
PATH=$PATH:~/.local/bin/

