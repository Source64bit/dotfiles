"
" init.vim
"

" ================ Plugin ================== "
" auto installing vim-plug 
" from LukeSmithxyz voidrice/.config/nvim/init.vim
" (https://github.com/LukeSmithxyz/voidrice/blob/master/.config/nvim/init.vim)

if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

" Calling Plugin 
call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))

Plug 'preservim/nerdtree'

call plug#end()

" =========== Configuration ============= "
    syntax on
    set number 
    set showcmd
    filetype indent on
    set showmatch
    set clipboard+=unnamedplus

" =========== Keybinds ============= "
" replace gj and gk with j and k
nnoremap j gj
nnoremap k gk

" replace : with space
nnoremap <Space> :

" replace Esc with jk   
imap jk <Esc>

" replace Esc with Tab on visual mode
vnoremap <Tab> <Esc>

" keybinds for nerdtree plugin
map <C-n> :NERDTreeToggle<CR>

" keybinds for terminal mode on vim
tnoremap <Esc> <C-\><C-n>

" press f to get rid search highlight on vim
nnoremap f :noh<CR>

" press zq to exit without saving
nnoremap zq ZQ

" auto source file tmux
autocmd BufWritePost ~/.tmux.conf !tmux source-file .tmux.conf





